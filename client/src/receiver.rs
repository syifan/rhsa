use crate::CONN;
use conn::msg::Msg;
use crossbeam_channel;
use crossbeam_channel::bounded;
use std::sync::{Arc, RwLock};
use std::thread;
use uuid::Uuid;

pub struct RspHandler {
    pub id: String,
    pub tx: crossbeam_channel::Sender<Msg>,
    pub rx: crossbeam_channel::Receiver<Msg>,
}

impl RspHandler {
    pub fn new() -> RspHandler {
        let (t, r) = bounded(0);
        RspHandler {
            id: Uuid::new_v4().to_string(),
            tx: t.clone(),
            rx: r.clone(),
        }
    }
}

type HandlerList = Arc<RwLock<Vec<Arc<RspHandler>>>>;

pub struct Receiver {
    handlers: HandlerList,
}

fn receiver_thread_fn() {
    dbg!("here");
    //loop {
    //dbg!("here");
    //let client = CLIENT.read().unwrap();
    //dbg!("here");
    //let handlers = client.receiver.handlers.read().unwrap();

    //dbg!("here");

    //let mut msg = client.recv().unwrap();
    //dbg!("here");

    //for h in handlers.iter() {
    //dbg!("here");
    //h.tx.send(msg).unwrap();
    //msg = h.rx.recv().unwrap();
    //}
    //}
}

impl Receiver {
    //pub fn new() -> Receiver {
        //Receiver {
            //handlers: Arc::new(RwLock::new(Vec::new())),
        //}
    //}

    pub fn spawn_receive_thread() -> Receiver {
        dbg!("here");
        thread::spawn(|| {
            dbg!("here");
            //receiver_thread_fn()
        })
        .join()
        .unwrap();

        Receiver {
            handlers: Arc::new(RwLock::new(Vec::new())),
        }
    }

    pub fn register_rsp_handler(&self, h: &Arc<RspHandler>) {
        self.handlers.write().unwrap().push(h.clone());
    }

    pub fn unregister_rsp_handler(&self, h: &Arc<RspHandler>) {
        let mut handlers = self.handlers.write().unwrap();
        let removed = handlers
            .iter()
            .position(|n| n.id == h.id)
            .map(|index| handlers.remove(index))
            .is_some();

        if !removed {
            panic!("handler not registered");
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn should_start_recv_thread() {
        Receiver::spawn_receive_thread();
    }
}
