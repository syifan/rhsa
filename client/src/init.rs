use crate::status::HSAStatus;
use crate::{CONN, RECEIVER};
use crate::receiver::RspHandler;
use crate::receiver::Receiver;
use conn::msg::{Msg, Payload, ReqInitConnection};
use std::sync::Arc;

#[no_mangle]
pub fn hsa_init() -> HSAStatus {
    println!("It works!");

    CONN.write().unwrap().send(&Msg { payload: Payload::ReqInitConnection(ReqInitConnection {}) }).unwrap();

    let _response = CONN.read().unwrap().recv().unwrap();

    //Receiver::spawn_receive_thread();

    //let receiver = RECEIVER.read().unwrap();

    //let rsp_handler = Arc::new(RspHandler::new());
    //receiver.register_rsp_handler(&rsp_handler);

    //let msg = Msg {
        //payload: Payload::ReqInitConnection(ReqInitConnection {}),
    //};
    //let conn = CONN.read().unwrap();
    //conn.send(&msg).unwrap();

    //let rsp = rsp_handler.rx.recv().unwrap();
    //dbg!(rsp);
    //receiver.unregister_rsp_handler(&rsp_handler);

    HSAStatus::Success
}
