mod agent;
mod init;
mod receiver;
mod status;

extern crate conn;
extern crate crossbeam_channel;
#[macro_use]
extern crate lazy_static;
extern crate uuid;

// use crate::client::Client;
use crate::receiver::Receiver;
use conn::Conn;
use std::sync::{Arc, RwLock};

lazy_static! {
  // static ref CLIENT:Arc<RwLock<Client>> =
  // Arc::new(RwLock::new(Client::new()));
  static ref CONN : Arc<RwLock<Conn>> = 
      Arc::new(RwLock::new(Conn::dail("127.0.0.1:32576").unwrap()));
  static ref RECEIVER : Arc<RwLock<Receiver>> = 
      Arc::new(RwLock::new(Receiver::spawn_receive_thread()));
}

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }
}
