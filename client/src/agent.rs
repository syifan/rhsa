extern crate libc;
use crate::status::HSAStatus;

#[repr(C)]
pub struct Agent {
    handle: u64,
}

#[no_mangle]
pub fn hsa_iterate_agents(
    callback: extern "C" fn(Agent, *mut libc::c_void) -> HSAStatus,
    data: *mut libc::c_void,
) -> HSAStatus {
    println!("Iterating agent");
    let agent = Agent { handle: 1234 };
    let ret = callback(agent, data);
    ret
}

#[no_mangle]
pub fn hsa_agent_get_info(agent: Agent, attribute: u32, value: *mut libc::c_void) -> HSAStatus {
    println!("Getting agent {} info {}", agent.handle, attribute);

    HSAStatus::Success
}
