# rhsa

## Compile

Assuming you heve cloned the rhsa repo in `$RHSA_ROOT`, run `cargo build` in `$RHSA_ROOT`.

## Run

After compiling rhsa, you should have a `target` folder in `$RHSA_ROOT`. In `$RHSA_ROOT/target/debug` folder, two binrary files are the final product of rhsa. One is the executable `server` and the other one is `libhsa_runtime64.so`. To start the server, in a terminal tab, run the server excutable by `./server`. To start a client, in a new terminal tab, first run `export LD_PRELOAD=$RHSA_ROOT/target/debug/lib_hsaruntime64.so` and then run your application.

The program in `$RHSA_ROOT/sample/vector_add` is a good starting point to run the code.
