#![allow(non_upper_case_globals)]
#![allow(non_camel_case_types)]
#![allow(non_snake_case)]

include!(concat!(env!("OUT_DIR"), "/bindings.rs"));

use conn::msg::{AgentInfo, Msg, Payload, RspInitConnection};
use conn::Conn;
use std::str;
use std::os::raw::{c_void, c_char};
use std::ffi::CString;
use std::thread::spawn;

#[derive(Clone)]
struct ClientHandler {
    gpus: Vec<AgentInfo>,
}

impl ClientHandler {
    fn server_response(&mut self, msg: Msg) -> Option<Msg> {
        match msg.payload {
            Payload::ReqInitConnection(_) => {
                Some(Msg { payload: Payload::RspInitConnection(RspInitConnection { gpus: self.gpus.clone() })})
            }
            _ => {
                None
            }
        }
    }
}

impl conn::ClientHandler for ClientHandler {
    fn handle(&mut self, conn: conn::Conn) {
        // TODO This server architecture has no way to mutate the server in response to messages
        let mut handler = self.clone();
        spawn(move || {
            println!("Client connected.");
            loop{
                let msg = conn.recv();
                match msg {
                    Ok(msg) => {
                        dbg!(&msg);
                        let rsp = handler.server_response(msg);
                        if let Some(rsp) = rsp {
                            conn.send(&rsp).unwrap();
                        }
                    },
                    Err(_) => break,
                };
            }
            //for msg in conn {
                //dbg!(&msg);
                //conn.send(&msg);
            //}
            println!("Client disconnected.");
        });
    }
}

#[derive(Debug)]
struct Server {
    gpus: Vec<AgentInfo>,
}

impl Server {
    fn init() -> Self {
        let server = Server {
            gpus: vec![],
        };

        let res = unsafe{hsa_init()};
        panic_on_error(res);

        server
    }

    fn iterate_agents(&mut self) {
        let res = unsafe { hsa_iterate_agents(Some(agent_callback), self as *mut _ as *mut c_void) };
        panic_on_error(res);
    }
}

extern "C" fn agent_callback(agent: hsa_agent_t, data: *mut c_void) -> hsa_status_t {
    let server: &mut Server = unsafe{&mut *(data as *mut Server)};

    let mut device_type:hsa_device_type_t = 0; 
    let res = unsafe{hsa_agent_get_info(agent, hsa_agent_info_t_HSA_AGENT_INFO_DEVICE, &mut device_type as *mut _ as *mut c_void)};
    panic_on_error(res);

    if device_type == hsa_device_type_t_HSA_DEVICE_TYPE_GPU {
        let mut agent_info = AgentInfo::new();

        let name_buf = vec![0;64];
        let res = unsafe{hsa_agent_get_info(agent, hsa_agent_info_t_HSA_AGENT_INFO_NAME, name_buf.as_ptr() as *mut c_void)};
        panic_on_error(res);
        let name = unsafe{CString::from_raw(name_buf.as_ptr() as *mut c_char)};
        agent_info.name = name.to_str().unwrap().to_string();

        let res = unsafe{hsa_agent_get_info(agent, hsa_agent_info_t_HSA_AGENT_INFO_VENDOR_NAME, name_buf.as_ptr() as *mut c_void)};
        panic_on_error(res);
        let name = unsafe{CString::from_raw(name_buf.as_ptr() as *mut c_char)};
        agent_info.vendor_name = name.to_str().unwrap().to_string();

        server.gpus.push(agent_info);
    }

    dbg!(server);

    hsa_status_t_HSA_STATUS_SUCCESS
}

fn panic_on_error(res: hsa_status_t) {
    if res != hsa_status_t_HSA_STATUS_SUCCESS {
        let mut err_name = [0;64];
        let query_res = unsafe{hsa_status_string(res, &mut err_name as *mut _ as *mut *const c_char)};

        if query_res != hsa_status_t_HSA_STATUS_SUCCESS{
            panic!("Unknown error");
        } else {
            panic!("{}", str::from_utf8(&err_name).unwrap());
        }
    }
}

fn main() {
    let mut server = Server::init();

    server.iterate_agents();

    Conn::listen("127.0.0.1:32576", &mut ClientHandler { gpus: server.gpus }).unwrap();
}


