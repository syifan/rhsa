//use serde::{Deserialize, Serialize};
//use serde_json::Result;

#[derive(Debug, Serialize, Deserialize)]
pub struct Msg {
    pub payload: Payload,
}

#[derive(Debug, Serialize, Deserialize)]
pub enum Payload {
    ReqInitConnection(ReqInitConnection),
    RspInitConnection(RspInitConnection),
}

#[derive(Debug, Serialize, Deserialize)]
pub struct ReqInitConnection {}

#[derive(Debug, Serialize, Deserialize)]
pub struct RspInitConnection {
    pub gpus: Vec<AgentInfo>,
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct AgentInfo {
    pub name: String,
    pub vendor_name: String,
}

impl AgentInfo {
    pub fn new() -> AgentInfo {
        AgentInfo{
            name: String::new(),
            vendor_name: String::new(),
        }
    }
}

#[cfg(test)]
mod test {
    use super::*;
    //use serde_json;
    #[test]
    fn serializtion() {
        let payload = Payload::ReqInitConnection(ReqInitConnection {});
        let msg = super::Msg { payload };
        let json = serde_json::to_string(&msg).unwrap();

        println!("{}", json);
    }
}
