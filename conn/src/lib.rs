#[macro_use]
extern crate serde_derive;
extern crate serde;
extern crate serde_json;
extern crate crossbeam_channel;

use std::net::{TcpListener, TcpStream};
//use std::thread;
use crate::msg::Msg;
use std::io;
use std::io::{Read, Write};
use std::mem::transmute;
use crossbeam_channel::{Sender, Receiver, bounded};

pub mod msg;

pub struct Conn {
    send_tx: Sender<TcpStream>,
    send_rx: Receiver<TcpStream>,
    recv_tx: Sender<TcpStream>,
    recv_rx: Receiver<TcpStream>,
    //send_stream: TcpStream,
    //recv_stream: TcpStream,
}

pub trait ClientHandler {
    fn handle(&mut self, conn: Conn);
}

fn u8_array_to_u32(array: &[u8; 4]) -> u32 {
    ((array[0] as u32) << 0)
        + ((array[1] as u32) << 8)
        + ((array[2] as u32) << 16)
        + ((array[3] as u32) << 24)
}

fn u32_to_u8_array(x: u32) -> [u8; 4] {
    unsafe { transmute(x.to_le()) }
}

impl Conn {
    fn new() -> Conn {
        let (send_tx, send_rx) = bounded(1);
        let (recv_tx, recv_rx) = bounded(1);
        Conn {
            send_tx, 
            send_rx,
            recv_tx,
            recv_rx,
        }
    }
    
    pub fn dail(addr: &str) -> io::Result<Conn> {
        let send_stream = TcpStream::connect(addr)?;
        let recv_stream = send_stream.try_clone()?;
        let conn = Conn::new();
        conn.send_tx.send(send_stream).unwrap();
        conn.recv_tx.send(recv_stream).unwrap();
        Ok(conn)
    }

    pub fn listen<T: ClientHandler>(addr: &str, client_handler: &mut T) -> io::Result<()> {
        let listener = TcpListener::bind(addr)?;
        for stream in listener.incoming() {
            let send_stream = stream?;
            let recv_stream = send_stream.try_clone()?;

            let conn = Conn::new();
            conn.send_tx.send(send_stream).unwrap();
            conn.recv_tx.send(recv_stream).unwrap();
            client_handler.handle(conn);
        }
        Ok(())
    }

    pub fn recv(&self) -> io::Result<Msg> {
        let mut stream = self.recv_rx.recv().unwrap();

        let mut buf = [0; 4];
        stream.read_exact(&mut buf)?;
        let msg_size = u8_array_to_u32(&buf);

        println!("Received size: {}", msg_size);

        let mut msg_buf = vec![0; msg_size as usize];
        stream.read_exact(&mut msg_buf)?;

        let msg: Msg = serde_json::from_slice(&msg_buf)?;

        println!("Received msg:\n{:?}", &msg);

        Ok(msg)
    }

    pub fn send(&self, msg: &Msg) -> io::Result<()> {
        let mut stream = self.send_rx.recv().unwrap();

        let msg_buf = serde_json::to_vec(&msg)?;
        let size_buf = u32_to_u8_array(msg_buf.len() as u32);

        stream.write(&size_buf)?;
        stream.write(&msg_buf)?;

        println!("Sent msg:\n{:?}", &msg);

        Ok(())
    }
}

impl Iterator for Conn {
    type Item = Msg;

    fn next(&mut self) -> Option<Self::Item> {
        match self.recv() {
            Err(_) => None,
            Ok(msg) => Some(msg),
        }
    }
}

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }
}
